'use strict';
$(document).ready(function () {
  mainSlider();
  menu();
  stickyHeader();
  slider();
  mobileMenu();
});

$(window).resize(function () {
  mainSlider();
  slider();
  mobileMenu();
});

function mainSlider() {
  let slider = $('.main-slider');
  if(!(slider.hasClass('slick-initialized'))) {
    slider.slick({
      speed: 500,
      infinite: false,
      fade: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: false,
      arrows: true,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            arrows: false,
            dots: true,
            infinite: true,
          }
        }
      ]
    });
  }
}

function menu() {
  $(window).scroll(function () {
    let scrollDistance = $(window).scrollTop();

    $('section').each(function (i) {
      if ($(this).position().top <= scrollDistance) {
        $('a[href*="#"]:not([href="#"]).active').removeClass('active');
        $('a').eq(i + 1).addClass('active');
      }
    });

  }).scroll();

  $(function () {
    $('a[href*="#"]:not([href="#"])').click(function () {

      if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
        let target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if($('nav').hasClass('show')) {
          $('nav').removeClass('show');
        }
        if (target.length) {
          $('html, body').animate({
            scrollTop: target.offset().top
          }, 500);
          return false;
        }
      }
    });
  });
}

function stickyHeader() {
  $(window).scroll(function () {
    let sticky = $('header'),
      scroll = $(window).scrollTop();

    if (scroll >= 100)
      sticky.addClass('fixed');
    else
      sticky.removeClass('fixed');
  });
}

function slider() {
  let slider = $('.half-wrap .list-wrap');
  if ($(window).width() < 768) {
    if(!(slider.hasClass('slick-initialized'))) {
      slider.slick({
        speed: 500,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: true,
        arrows: false
      });
    }
  } else {
    if(slider.hasClass('slick-initialized')) {
      slider.slick('unslick');
    }
  }
}

function mobileMenu() {
  let openBtn = $('.burgerIco');
  let closeBtn = $('.closeMenu');
  let menu = $('header nav');

  closeBtn.on('click', function (e) {
    menu.removeClass('show')
  });

  openBtn.on('click', function (e) {
    menu.addClass('show')
  });

}
